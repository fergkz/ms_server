<?php

function _isMode($var){ return isset($GLOBALS['config']['directories']['mode'][strtoupper($var)]['path']); }
function _isModeRestrict($var){ return @$GLOBALS['config']['directories']['mode'][strtoupper($var)]['restrict']; }
function _getModePath($var, $full=true){ return @($full?path.ds:"").rtrim(@$GLOBALS['config']['directories']['mode'][$var?strtoupper($var):"F"]['path'],"\\..\/"); }
function _getModeDescr($var){ return @$GLOBALS['config']['directories']['mode'][strtoupper($var)]['description']; }
function _getModeLevel($var){ return @$GLOBALS['config']['directories']['mode'][strtoupper($var)]['level']; }
function _getModeList(){
    foreach( $GLOBALS['config']['directories']['mode'] as $ind => $data ){
        $tmpNv[$ind] = @$GLOBALS['config']['modes'][$ind]['level'];
    }
    asort($tmpNv);
    foreach( $tmpNv as $tm => $tr ){ $m[] = $tm; }
    return $m; 
}

function _isModuleSitEditable($var){ return @$GLOBALS['config']['modules']['status.'.strtoupper($var).'.grant_change']; }
function _getModuleSitDescr($var){ return @$GLOBALS['config']['modules']['status.'.strtoupper($var).'.description']; }

function _getSubModuleActionDescr($var){ return @$GLOBALS['config']['modules']['sub_action.'.strtoupper($var).'.description']; }

function _getSubModuleTypeDescr($var){ return @$GLOBALS['config']['modules']['sub_type.'.strtoupper($var).'.description']; }

function _getAmbient(){ return @$GLOBALS['config']['ambient']["type"]; }
function _getAmbientDescr(){ return @$GLOBALS['config']['ambient']["description"]; }
function _getEmailConfig($param){ return @$GLOBALS['config']['email'][$param]; }
function _getPasswordDefaultCrypt(){ return @$GLOBALS['config']['password']["crypt"]; }
function _getEmailSiteName(){ return @$GLOBALS['config']['client']["email_name"]; }
function _getClientFilesPath(){ return @path.ds.rtrim($GLOBALS['config']['files']["path"],"\\..\/"); }