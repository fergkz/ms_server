<?php

namespace Core\System;

class DataBaseConnection{
    
    private $server;
    private $dbName;
    private $user;
    private $pass;
    private $type;
    private $conectionObject;
    private $bind = array();
    public  $erroMessage;
    
    public function __construct($transactionId=null){
        $this->server   = $GLOBALS['config']['database']['server'];
        $this->dbName   = $GLOBALS['config']['database']['db_name'];
        $this->user     = $GLOBALS['config']['database']['user'];
        $this->pass     = $GLOBALS['config']['database']['password'];
        $this->type     = $GLOBALS['config']['database']['type'];
        $string         = $this->type.":host=".$this->server.";dbname=".$this->dbName;
        $GLOBALS['cont'] = isset($GLOBALS['cont']) ? $GLOBALS['cont'] : 0;
        $GLOBALS['cont']++;
        
        $transactionId = $transactionId ? $transactionId : "GLOBAL";
        if( isset($GLOBALS['TRANSACTION']['CONNECTION'][$transactionId]) && $GLOBALS['TRANSACTION']['CONNECTION'][$transactionId] ){
            $this->conectionObject = &$GLOBALS['TRANSACTION']['CONNECTION'][$transactionId];
        }else{
            try{
                $this->conectionObject = new \PDO($string, $this->user, $this->pass) or die("Não foi possível conectar ao banco de dados.");
                $this->conectionObject->setAttribute(\PDO::ATTR_PERSISTENT, true);
                $this->conectionObject->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING);
                $this->conectionObject->exec("set names utf8");
                $this->conectionObject->beginTransaction();

                $GLOBALS['TRANSACTION']['CONNECTION'][$transactionId] = &$this->conectionObject;
            }catch( \PDOException $e ){
                _includeError("sqlConnect");
            }
        }
    }
    
    protected function setError( $erro ){
        if( is_object($erro) ){
            $this->erroMessage = $erro->getMessage();
            $this->erroCode    = $erro->getCode();
        }else{
            $this->erroMessage = $erro;
            $this->erroCode    = "0";
        }
//        _setError($this->erroMessage);
    }
    
    public function query( $sql, $bind = null ){
        unset($this->res);
        $this->bind = count($bind) > 0 ? $bind : null;
        $stmt = $this->conectionObject->prepare($sql);
        $this->res = @$stmt->execute($this->bind);
        if( $this->res ){
            $this->res = $stmt->fetchAll();
            return $this->res;
        }else{
            if( $stmt->errorCode() ){
                $erro = $stmt->errorInfo();
                $this->setError($erro[2]);
                return false;
            }
        }
    }
    
    public function execute( $sql, $bind = null ){
        @$bind = count($bind) > 0 ? $bind : null;
        @$stmt = $this->conectionObject->prepare($sql);
        @$ret = $stmt->execute($bind);
        if( @$ret ){
            return true;
        }else{
            $erro = $stmt->errorInfo();
            $this->setError($erro[2]);
            return false;
        }
        
    }
    
    public function lastId(){
        return $this->conectionObject->lastInsertId();
    }
    
    public function commit(){
        if( !_getErrors() && $this->conectionObject->inTransaction() ){
            $this->conectionObject->commit();
        }
    }
    
    public function rollback(){
        if( $this->conectionObject->inTransaction() ){
            $this->conectionObject->rollback();
        }
    }
 
    public function getTableInfoMysql( $table, $loadIndexs = null ){
        unset($this->res);
        $sql = "SHOW COLUMNS FROM {$table}";
        $stmt = $this->conectionObject->prepare($sql);
        $this->res = $stmt->execute();
        if( $this->res ){
            $dados = array();
            $this->res = $stmt->fetchAll();
            foreach( $this->res as $row ){
                $tmp = @explode("(",$row['Type']);
                $type = @$tmp[0];
                $tmp = @explode(")",$tmp[1]);
                $length = $tmp[0];
                
                $arr = array();
                $arr['Attribute']     = $row['Field'];
                $arr['Name']          = $row['Field'];
                $arr['Label']         = $row['Field'];
                $arr['Primary']       = @$row['Key'] == "PRI" ? 1 : 0;
                $arr['Null']          = @$row['Null'] == "YES" ? 1 : 0;                            
                $arr['Length']        = $length;
                $arr['AutoIncrement'] = @$row['Extra'] == "auto_increment" ? 1 : 0;
                $arr['Default']       = $row['Default'];

                $arr['Unique']        = @$row['Key'] == "UNI" or $row['Key'] == "PRI" ? 1 : 0;
                $arr['Reference'] = array(
                    'Table' => null,
                    'Column' => null
                );

                switch( $type ){
                    case "int":
                        $arr['Type'] = "Integer";
                        break;
                    case "char":
                        $arr['Type'] = "Char";
                        break;
                    case "varchar":
                        $arr['Type'] = "String";
                        break;
                    case "char":
                        $arr['Type'] = "Char";
                        break;
                    case "longtext":
                        $arr['Type'] = "CLOB";
                        break;
                    case "longblob":
                        $arr['Type'] = "BLOB";
                        break;
                    case "date":
                        $arr['Type'] = "Date";
                        break;
                    case "datetime":
                        $arr['Type'] = "DateTime";
                        break;
                }
                
                $dados[$row['Field']] = $arr;
            }
            
            return $dados;
        }
        return false;
    }
    
}