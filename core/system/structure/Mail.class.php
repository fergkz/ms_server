<?php

namespace Core\System;

use MVC\View as View;

class Mail{

    public $phpmailer;
    private $to = array();
    private $sendToFile = false;
    
    private $filename = "";
    
    public $errorMessage = "";
    
    public function __construct(){
        
        _includeLib("PHPMailer/PHPMailer.class.php");
        
        $config = &$GLOBALS['config']['email'];
        
        if( isset($config['send_to_file']) && $config['send_to_file'] ){
            $this->sendToFile = true;
            $this->filename = path.ds.$config['send_to_file'];
        }

        $this->phpmailer = new \PHPMailer();
        
        if( @!$config['host_smtp'] || !$config['username'] || !$config['password'] || !$config['port'] || !$config['sender_email'] ){
            return;
        }

        $this->phpmailer->SetLanguage(@$config['language']);
        $this->phpmailer->Host       = @$config['host_smtp'];
        $this->phpmailer->SMTPAuth   = @$config['smtp_auth'];
        $this->phpmailer->Username   = @$config['username'];
        $this->phpmailer->Password   = @$config['password'];
        $this->phpmailer->SMTPDebug  = @$config['debug'];
        $this->phpmailer->CharSet    = @$config['charset'];

        $this->phpmailer->Port       = @$config['port'];
        $this->phpmailer->SMTPAuth   = @$config['smtp_auth'];
        $this->phpmailer->SMTPSecure = @$config['smtp_secure'];

        $this->phpmailer->From       = @$config['sender_email'];
        $this->phpmailer->FromName   = @$config['sender_name'];

        $this->phpmailer->IsHTML(@$config['is_html']);
        if( @$config['is_smtp'] ){
            $this->phpmailer->IsSMTP();
        }

    }

    public function setAddress( $email, $name = null ){
        if( $this->phpmailer ){
            $this->phpmailer->AddAddress($email, $name);
            $this->to[] = $email;
        }
        return $this;
    }

    public function setMessage( $msg ){
        if( $this->phpmailer ){
            $this->phpmailer->Body = $msg;
        }
        return $this;
    }
    
    public function setMessageTemplate( $filename, $render = array() ){
        if( $this->phpmailer ){
            $View = new View();
            $View->setTemplate($filename);
            $this->phpmailer->Body = $View->render($render);
        }
    }

    public function setSubject( $subject ){
        if( $this->phpmailer ){
            $this->phpmailer->Subject = $subject;
        }
        return $this;
    }

    public function send(){
        
        if( !$this->phpmailer ){
            return false;
        }
        if( !$this->phpmailer->From ){
            $this->errorMessage = _translate("hz_mail_from");
            return false;
        }
        if( !$this->phpmailer->Subject ){
            $this->errorMessage = _translate("hz_mail_subject");
            return false;
        }
        if( !$this->phpmailer->Body ){
            $this->errorMessage = _translate("hz_mail_body");
            return false;
        }
        
        if( !$this->sendToFile ){
            $res = $this->phpmailer->Send();
            if( !$res ){
                $this->errorMessage = _translate("hz_mail_fail");
                return false;
            }
        }else{
            return $this->fileBurn();
        }
        return true;
    }
    
    private function fileBurn(){
        try{
            if( !file_exists($this->filename) ){
                $handle = fopen($this->filename, "a");
                fclose($handle);
            }
            $content = file_get_contents($this->filename);
            $list = $content ? json_decode($content) : array();
            $nw['from'] = $this->phpmailer->From;
            $nw['body'] = $this->phpmailer->Body;
            $nw['subject'] = $this->phpmailer->Subject;
            $nw['to'] = $this->to;
            $nw['date'] = date('Y-m-d H:i:s');
            array_unshift($list, $nw);
            file_put_contents($this->filename, json_encode($list));
        }catch( Exception $e ){
            debug("change");
        }
        return true;
    }
    
}