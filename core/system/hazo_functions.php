<?php

use MVC\Session as Session;

function _query($sql,$bind=null){
    $db = new Core\System\DataBaseConnection();
    return $db->query($sql,$bind);
}

function _commit(){
    if( !_getErrors() ){
        $db = new Core\System\DataBaseConnection();
        return $db->commit();
    }
    return false;
}
function _rollback(){
    $GLOBALS['TRANSACTION']['ERRORS'] = true;
    if( @$GLOBALS['CONFIG']['DATABASE']['TRANSACTION']['IN'] ){
        $db = new Core\System\DataBaseConnection();
        return $db->rollback();
    }
    return false;
}
function debug( $var, $noDie = null, $title = null ){
    $GLOBALS['TRANSACTION']['ERRORS'] = true;
    echo "<div style='border:1px solid #000;padding:10px;'>";
    if( $title ){
        echo "<div style='padding:3px 8px;background-color:#000;color:#DDD;'>".strtoupper($title)."</div>";
    }
    echo "<pre style='font-family: monospace'>";
    print_r($var);
    echo "</pre>";
    echo "</div>";
    _clearErrors();
    _rollback();
    if( !$noDie ){
        die;
    }
}
function _translate( $mode, $code = null, $replace = array(), $message = null ){
    
    if( $replace && is_array($replace) && count($replace) > 0 ){
        $pChange = $replace;
        $pFind = array_flip($replace);
    }else{
        $pChange = $pFind = array();
    }
    
    if( $code ){
        $filename = "";
        if( !empty($GLOBALS['config']['define']['translate_file']) ){
            $filename = $GLOBALS['config']['define']['translate_file'];
            if( substr(trim($filename, "\\.\/"),0,  strlen(path)) == path ){
                $filename = path.ds.substr(trim($filename, "\\.\/"), strlen(path));
            }else{
                if( path.ds.trim($filename, "\\.\/") ){
                    $filename = path.ds.trim($filename, "\\.\/");
                }
            }
        }
        if( file_exists($filename) ){
            $configs = parse_ini_file($filename, true);

            if( !empty($configs[$mode]) ){
                if( !empty($configs[$mode][$code]) ){
                    return str_ireplace($pFind, $pChange, trim($configs[$mode][$code]));
                }
            }
        }
        
        $filename = dirname(__DIR__).DS."translate.ini";
        $configs2 = parse_ini_file($filename, true);
        if( !empty($configs2[$mode]) ){
            if( !empty($configs2[$mode][$code]) ){
                return str_replace($pFind, $pChange, trim($configs2[$mode][$code]))."";
            }
        }
        
        if( !$message ){
            if( !empty($configs[$mode]['hz_error_not_found']) ){
                return str_replace($pFind, $pChange, trim(@$configs[$mode]['hz_error_not_found']));
            }else{
                return str_replace($pFind, $pChange, trim(@$configs2[$mode]['hz_error_not_found']));
            }
        }
    }
    
    if( $message ){
        return str_replace($pFind, $pChange, trim($message));
    }
    
    return "";
}
function _setError($var, $replace = array()){
    if( $var ){
        $msgs = Session::get("transaction_msg_errors") ?: array();
        $msgs[] = _translate('errors', $var, $replace, $var);
        Session::set("transaction_msg_errors", $msgs);
    }
    $GLOBALS['TRANSACTION']['ERRORS'] = true;
    _setScrollPrev();
}
function _raise($erro=null, $replace = array()){
    $GLOBALS['TRANSACTION']['ERRORS'] = true;
    $db = new Core\System\DataBaseConnection();
    $db->rollback();
    throw new Exception(_translate("errors", $erro, $replace, $erro) );
    return false;
}
function _getErrors(){
    return Session::get("transaction_msg_errors");
}
function _clearErrors(){
    Session::del("transaction_msg_errors");
}
function _setAlert($var, $replace = array()){
    if( $var ){
        $msgs = Session::get("transaction_msg_alerts") ?: array();
        $msgs[] = _translate('alerts', $var, $replace, $var);
        Session::set("transaction_msg_alerts", $msgs);
    }
    _setScrollPrev();
}
function _getAlert(){
    return Session::get("transaction_msg_alerts");
}
function _clearAlert(){
    Session::del("transaction_msg_alerts");
}
function _setSuccess($var, $replace = array()){
    if( $var ){
        $msgs = Session::get("transaction_msg_success") ?: array();
        $msgs[] = _translate('success', $var, $replace, $var);
        Session::set("transaction_msg_success", $msgs);
    }
    _setScrollPrev();
}
function _getSuccess(){
    return Session::get("transaction_msg_success");
}
function _clearSuccess(){
    Session::del("transaction_msg_success");
}
function _setInfo($var, $replace = array()){
    if( $var ){
        $msgs = Session::get("transaction_msg_infos") ?: array();
        $msgs[] = _translate('infos', $var, $replace, $var);
        Session::set("transaction_msg_infos", $msgs);
    }
    _setScrollPrev();
}
function _getInfo(){
    return Session::get("transaction_msg_infos");
}
function _clearInfo(){
    Session::del("transaction_msg_infos");
}
function _setScrollPrev($url=true){
    Session::set("transaction_prev_scroll", $url);
}
function _clearScrollPrev(){
    Session::del("transaction_prev_scroll");
}
function _getScrollPrev(){
    return Session::get("transaction_prev_scroll");
}
function _shutdownExecuteCommit(){
    if( @$GLOBALS['TRANSACTION']['CONNECTION'] ){
        foreach( $GLOBALS['TRANSACTION']['CONNECTION'] as $id => $obj ){
            $db = new Core\System\DataBaseConnection($id);
            if( !@$GLOBALS['TRANSACTION']['ERRORS'] && !@$GLOBALS['CONFIG']['DATABASE']['TRANSACTION']['IN'] ){
                $db->commit();
            }
        }
    }
}