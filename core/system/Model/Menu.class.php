<?php

namespace Core\Model;

use MVC\Model as Model;
use Core\System\Module as Module;
use MVC\Session as Session;

class Menu extends Model{

    protected $ID;
    protected $mode;
    protected $module;
    protected $submodule;
    protected $description;
    protected $parent;
    protected $parentObj;
    protected $groupID;
    protected $sequence;
    protected $descriptionRoute;
    protected $level;
    protected $chields;
    protected $loadWithChields = false;

    public function setID( $ID ){
        $this->ID = $ID;
        return $this;
    }

    public function getID(){
        return $this->ID;
    }

    public function setMode( $mode ){
        $this->mode = $mode;
        return $this;
    }

    public function getMode(){
        return $this->mode;
    }

    public function setModule( $module ){
        $this->module = $module;
        return $this;
    }

    public function getModule(){
        return $this->module;
    }

    public function setSubmodule( $submodule ){
        $this->submodule = $submodule;
        return $this;
    }

    public function getSubmodule(){
        return $this->submodule;
    }

    public function setDescription( $description ){
        $this->description = $description;
        return $this;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setParent( $parent ){
        $this->parent = $parent;
        return $this;
    }

    public function getParent(){
        return $this->parent;
    }

    public function setParentObj( $parentObj ){
        $this->parentObj = $parentObj;
        $this->parent = $parentObj->getID();
        return $this;
    }

    public function getParentObj(){
        if( !$this->parentObj ){
            $this->parentObj = Menu::getInstance($this->parent);
        }
        return $this->parentObj;
    }

    public function setGroupID( $groupID ){
        $this->groupID = $groupID;
        return $this;
    }

    public function getGroupID(){
        return $this->groupID;
    }

    public function setSequence( $sequence ){
        $this->sequence = $sequence;
        return $this;
    }

    public function getSequence(){
        return $this->sequence;
    }

    public function setDescriptionRoute( $descriptionRoute ){
        $this->descriptionRoute = $descriptionRoute;
        return $this;
    }

    public function getDescriptionRoute(){
        return $this->descriptionRoute;
    }

    public function setChield( $Menu ){
        if( $this->loadWithChields ){
            $Menu = is_object($Menu) ? $Menu : Menu::getByID($Menu);
            $Menu->loadWithChields()->load();
        }else{
            $Menu = is_object($Menu) ? $Menu : Menu::getByID($Menu);
        }
        $this->chields[$Menu->getSequence()] = $Menu;
        return $this;
    }

    public function getChields(){
        return $this->chields;
    }

    public function getLevel(){
        return $this->level;
    }

    public function loadWithChields(){
        $this->loadWithChields = true;
        return $this;
    }

    protected function triggerBeforeInsertUpdate(){
        if( !$this->new->sequence ){
            $sql = "select max(sequence) + 1 as nseq from hazo_menu where group_id = :group_id";
            $bind['group_id'] = $this->new->groupID;
            $res = _query($sql, $bind);
            $this->new->sequence = _coalesce($res[0]['nseq'], '1');
        }
        if( !$this->new->description && $this->new->mode && $this->new->module && $this->new->submodule ){
            $Module = new Module();
            $Module->setMode($this->new->mode);
            $Module->setModule($this->new->module);
            $Module->load();
            $Submodule = $Module->getSubmodules($this->new->submodule);
            $this->new->description = $Submodule->getDescription();
        }
    }

    protected function triggerAfterLoad(){
        $this->level = $this->calcLevel();
        if( $this->loadWithChields ){
            $sql = "select id from hazo_menu where parent = {$this->ID} order by sequence";
            $res = _query($sql);
            foreach( $res as $row ){
                $this->loadWithChields();
                $this->setChield($row['id']);
            }
        }
    }

    protected function calcLevel( $menuID = null ){
        $Menu = _coalesce($menuID, $this);
        if( !$Menu->parent ){
            return '1';
        }else{
            return 1 + $this->calcLevel($Menu->getParentObj());
        }
    }

    public function moveUp(){
        return $this->moveSequence('U');
    }

    public function moveDown(){
        return $this->moveSequence('D');
    }

    private function moveSequence( $action = null ){
        if( !$action || !in_array($action, array( "U", "D" )) ){
            return false;
        }
        $parent = $this->parent ? "and parent = ".$this->parent : "and parent is null";
        if( !$this->sequence ){
            $this->sequence = 1;
        }
        if( $action == 'U' ){
            $sql = "select max(sequence) as sequence from hazo_menu where sequence < {$this->sequence} and group_id = {$this->groupID} $parent";
        }else{
            $sql = "select min(sequence) as sequence from hazo_menu where sequence > {$this->sequence} and group_id = {$this->groupID} $parent";
        }
        $res = _query($sql);
        $lastMenuID = false;
        if( $res[0]['sequence'] ){
            $lastSequence = $res[0]['sequence'];
            $sql = "select id from hazo_menu where sequence = $lastSequence and group_id = {$this->groupID} $parent";
            $res = _query($sql);
            $lastMenuID = $res[0]['id'];
        }
        if( $lastMenuID && $lastMenuID != $this->ID ){
            $res = _query("select max(sequence)+10 as sequence from hazo_menu");
            $maxSequence = $res[0]['sequence'];
            $mySequence = $this->sequence;
            $this->setSequence($maxSequence)->save();
            $MenuOld = Menu::getInstance($lastMenuID)->setSequence($mySequence)->save();
            $this->setSequence($lastSequence)->save();
        }
    }
    
    public static function getBranchByGroup($Group=null){
        if( Session::get("transaction_menu_branch") ){
            return Session::get("transaction_menu_branch");
        }
        $Group = is_object($Group) ? $Group : Group::getById($Group);
        
        $sql = "select id from hazo_menu where group_id = :group_id and parent is null order by sequence";
        $bind['group_id'] = $Group->getID();
        $res = _query($sql, $bind);
        $dados = array();
        foreach( $res as $row ){
            $dados[] = Menu::getInstance()->loadWithChields()->setID($row['id'])->load();
        }
        Session::set("transaction_menu_branch", (count($dados) > 0 ? $dados : false));
        return count($dados) > 0 ? $dados : false;
    }

    public static function listByGroup( $groupID = null, $parentID = null ){
        if( !$groupID ){
            return false;
        }
        if( $parentID ){
            $whereParent = " and parent = :parent ";
            $bind['parent'] = $parentID;
        }else{
            $whereParent = " and parent is null";
        }
        $sql = "select id from hazo_menu where group_id = :group_id {$whereParent} order by sequence, id";
        $bind['group_id'] = $groupID;
        $res = _query($sql, $bind);
        $dados = array( );
        foreach( $res as $row ){
            $Menu = Menu::getInstance($row['id']);
            $dados[] = $Menu;
            $chieldsMenu = self::listByGroup($groupID, $row['id']);
            if( $chieldsMenu && is_array($chieldsMenu) ){
                $dados = array_merge($dados, $chieldsMenu);
            }
        }
        return count($dados) > 0 ? $dados : false;
    }

    public static function deleteAll( $Menu ){
        $Menu = is_object($Menu) ? $Menu : Menu::getByID($Menu);
        if( !$Menu ){
            return false;
        }
        $sql = "select id from hazo_menu where parent = :parent";
        $bind['parent'] = $Menu->getID();
        $res = _query($sql, $bind);
        foreach( $res as $row ){
            if( !Menu::deleteAll($row['id']) ){
                return false;
            }
        }
        return $Menu->save("D");
    }

}