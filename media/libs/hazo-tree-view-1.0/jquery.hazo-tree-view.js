jQuery.fn.hazoTreeView = function(jsonParam) {
    var j = jQuery.noConflict();

    var minSb = function(element) {
        j(element).parent().find("> .tree-icon-toggle").text(" + ");
        j(element).data('show-sb', false);

        j(element).parent().find("> ul").each(function() {
            j(this).data('show-sb', false).hide();
            j(this).find('a').each(function() {
                minSb(this);
            });
        });
    };

    var maxSb = function(element) {
        j(element).data('show-sb', true);

        j(element).parent().find("> .tree-icon-toggle").text(" - ");
        j(element).parent().find("> ul").each(function() {
            j(this).data('show-sb', true).show();
        });
    };

    var toggleSb = function(element) {

        if (typeof j(element).data('show-sb') === "undefined" || j(element).data('show-sb') === false) {
            maxSb(element);
        } else {
            minSb(element);
        }

    };

    return this.each(function() {

        if (typeof jsonParam !== 'undefined') {
            if (typeof jsonParam.prefix !== 'undefined' && jsonParam.prefix) {
                var id = 0;
                j(this).find('li').each(function() {
                    id++;
                    j(this).attr("id", jsonParam.prefix + "-" + id);
                });
            }
        }

        j(this).find('> li > a').each(function() {
            var element = this;
            j(element).addClass('tree-link-toggle');

            if (window.location.toString() === j(this).attr('href')) {
                maxSb(j(this));

                var li = j(this).closest("li");

                while (j(li).parent().closest("li").length > 0) {
                    var max = j(li).parent().closest("li").find("> .tree-link-toggle").first();

                    maxSb(max);

                    li = j(li).parent().closest("li").first();
                }
            }

            j(this).parent().find('> ul').hazoTreeView();

            var a;
            if (j(this).parent().find("> ul").length > 0) {
                a = j("<a class='tree-icon-toggle'> - </a>")
                        .css("display", "inline-block")
                        .css("cursor", "pointer")
                        .on("click", function() {
                            toggleSb(element);
                        });
            } else {
                a = j("<div class='tree-empty-toggle'>&nbsp;</div>").css("display", "inline-block");
            }
            j(this).before(a);
        });

        j(this).addClass("treev-hz");

        j(this).find("> li > a.tree-link-toggle").each(function() {
            if (typeof j(this).data('show-sb') === "undefined" || j(this).data('show-sb') === false) {
                minSb(this);
            }
        });

        return j(this);
    });
};