<?php

namespace Model;

class RconCommand
{
    private static $rcon;
    public static $connection;
    
    private static function connect()
    {
//        $host = '127.0.0.1';                // Server host name or IP
//        $port = 8090;                       // Port rcon is listening on
//        $password = 'bnkv41';               // rcon.password setting set in server.properties
//        $timeout = 1;                       // How long to timeout.
//        
//        self::$rcon = new Rcon($host, $port, $password, $timeout);
//        self::$rcon->connect();
    }
    
    public static function sendOld( $cmd )
    {
        require_once __DIR__ . '/SourceQuery/SourceQuery.class.php';
        
        ob_start();
        
//        header( 'Content-Type: text/plain' );
        define( 'SQ_SERVER_ADDR', '127.0.0.1' );
        define( 'SQ_SERVER_PORT', 8090 );
        define( 'SQ_TIMEOUT',     1 );
        define( 'SQ_ENGINE',      \SourceQuery :: SOURCE );
        
        $Query = new \SourceQuery( );
        
        $Query->Connect( SQ_SERVER_ADDR, SQ_SERVER_PORT, SQ_TIMEOUT, SQ_ENGINE );
		
		$Query->SetRconPassword( 'bnkv41' );
        
        $response = $Query->Rcon( $cmd );
        $Query->Disconnect();
        ob_end_clean();
        
        return $response;
        
//        self::connect();
//        $response = self::$rcon->send_command( $cmd );
//        self::$rcon->disconnect();
//        socket_close( self::$rcon->socket );
//        return $response;
    }
    
    public static function send( $ms_command )
    {
        $filename = '/var/servidor/logs/latest.log';
        
        $codehash = md5( time().rand(111111111111, 999999999999) );
        
        $cmd_ini = "pardon {$codehash}\n";
        $cmd_end = "\npardon {$codehash}";
        
        $cmd = 'screen -S ms_server -p 0 -X stuff "'.$cmd_ini.$ms_command.$cmd_end.'$(printf \\\r)"';
        
        self::shell($cmd);
        sleep(1);
        
        $start = false;
        $res = array();
        foreach( explode("\n", file_get_contents($filename)) as $row ){
            if( !$row ){
                continue;
            }
            if( $start ){
                if( stristr($row, $codehash) ){
                    break;
                }
                $res[] = $row;
            }
            if( !$start && stristr($row, $codehash) ){
                $start = true;
            }
        }
        
        $return = array();
        foreach( $res as $row ){
            $tmp = explode("]:", $row);
            $return[] = trim( end($tmp) );
        }
        
        return implode("\n", $return);
    }
    
    public static function shell( $cmd )
    {
        if( empty( self::$connection ) ){
            self::$connection = ssh2_connect("127.0.0.1");
            ssh2_auth_password(self::$connection,"root", "minecrafterson");
        }
        
        // Run a command that will probably write to stderr
        $stream = ssh2_exec(self::$connection, $cmd);
        $errorStream = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
        
        // Enable blocking for both streams
        stream_set_blocking($errorStream, true);
        stream_set_blocking($stream, true);
        
        $output = stream_get_contents($stream);
        $error = stream_get_contents($errorStream);
        
        fclose($errorStream);
        fclose($stream);
        
        return array(
            "output" => $output,
            "error" => $error
        );
    }
}