<?php

namespace Model;

class PlayerModel
{
    public function __construct( $playername )
    {
        $path = $GLOBALS['config']['minecraft']['server_path'];
        $path_playerdata = $path.'world'.ds.'playerdata';
        RconCommand::send("save-all");
        
        $nbt = null;
        
        foreach( json_decode(file_get_contents($path.'usercache.json')) as $sv_jog ){
            if( $sv_jog->name !== $playername ){
                continue;
            }
            $nbt = new \Model\MinecraftPlayerNBT($path_playerdata.ds.$sv_jog->uuid.".dat");
            
            break;
        }
        
        if( $nbt ){
            
            $this->playername = $playername;
            
            $tmp_post = $nbt->getProperty("Pos");
            $this->position = array(
                "x" => $tmp_post['value'][0],
                "y" => $tmp_post['value'][1],
                "z" => $tmp_post['value'][2]
            );
        
            $this->inventory = array();
            $tmp_inv = $nbt->getProperty("Inventory");
            foreach( $tmp_inv['value'] as $row ){
                $this->inventory[] = array(
                    'id' => str_replace("minecraft:", "", $row[0]['value']),
                    'dano' => $row[1]['value'],
                    'qtde' => $row[2]['value'],
                    'slot' => $row[3]['value']
                );
            }
        }
    }
}