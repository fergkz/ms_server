<?php

namespace Model;

use System\MySession as MySession;

class UsuarioModel extends \System\MyModel
{
    protected static $daoTable   = "sis_usuario";
    protected static $daoPrimary = array('id' => 'id');
    protected static $daoCols    = array(
        'id'     => 'id',
        'login'  => 'login',
        'senha'  => 'senha',
        'nome'   => 'nome',
        'email'  => 'email',
        'status' => 'status',
        'tipo'   => 'tipo',
    );
    
    protected $id;
    protected $login;
    protected $senha;
    protected $nome;
    protected $email;
    protected $status;
    protected $tipo;

    public function getId(){
        return $this->id;
    }

    public function getLogin(){
        return $this->login;
    }

    public function getNome(){
        return $this->nome;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getStatus(){
        return $this->status;
    }

    public function getTipo(){
        return $this->tipo;
    }

    public function setId( $id ){
        $this->id = $id;
    }

    public function setLogin( $login ){
        $this->login = $login;
    }

    public function setSenha( $senha ){
        $this->senha = md5($senha);
    }

    public function setNome( $nome ){
        $this->nome = $nome;
    }

    public function setEmail( $email ){
        $this->email = $email;
    }

    public function setStatus( $status ){
        $this->status = $status;
    }

    public function setTipo( $tipo ){
        $this->tipo = $tipo;
    }
    
    public static function getExistente( $login, $senha )
    {
        $usuarios = UsuarioModel::getList(array(
            "dao.login = ?" => $login,
            "dao.senha = ?" => md5($senha),
            "dao.status = 'A'"
        ), null, 0, 1);
        
        if( $usuarios['cont_total'] > 0 ){
            return $usuarios['rows'][0];
        }else{
            return false;
        }
    }
    
    public static function login( $login, $senha )
    {
        $Usuario = UsuarioModel::getExistente($login, $senha);
        
        if( $Usuario){
            MySession::set("usuario_online", $Usuario);
            return true;
        }else{
            return false;
        }
    }
    
    public static function logoff()
    {
        MySession::set("usuario_online", null);
    }
    
    public static function getOnline()
    {
        return MySession::get("usuario_online");
    }
    
    public function triggerBeforeSave()
    {
        if( $this->daoAction === "I" ){
            RconCommand::send("whitelist add {$this->login}");
        }
    }

    public function triggerAfterSave()
    {
        
    }

}