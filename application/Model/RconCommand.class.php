<?php

namespace Model;

class RconCommand
{
    private static $rcon;
    
    private static function connect()
    {
        $host = '127.0.0.1';                // Server host name or IP
        $port = 8090;                       // Port rcon is listening on
        $password = 'minecrafterson';       // rcon.password setting set in server.properties
        $timeout = 1;                       // How long to timeout.
        
        self::$rcon = new Rcon($host, $port, $password, $timeout);
        self::$rcon->connect();
    }
    
    public static function send( $cmd )
    {
        self::connect();
        $response = self::$rcon->send_command( $cmd );
        self::$rcon->disconnect();
        socket_close( self::$rcon->socket );
        return $response;
    }
}