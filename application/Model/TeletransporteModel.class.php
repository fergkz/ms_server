<?php

namespace Model;

class TeletransporteModel extends \System\MyModel
{
    protected static $daoTable   = "loja_teletransporte";
    protected static $daoPrimary = array('id' => 'id');
    protected static $daoCols    = array(
        'id'          => 'id',
        'usuarioId'   => 'usuario_id',
        'nome'        => 'nome',
        'coordenadas' => 'coordenadas',
    );
    
    protected $id;
    protected $usuarioId;
    protected $nome;
    protected $coordenadas;

    public function getId(){
        return $this->id;
    }

    public function getUsuarioId(){
        return $this->usuarioId;
    }

    public function getNome(){
        return $this->nome;
    }

    public function getCoordenadas(){
        return $this->coordenadas;
    }

    public function setId( $id ){
        $this->id = $id;
    }

    public function setUsuarioId( $usuarioId ){
        $this->usuarioId = $usuarioId;
    }

    public function setNome( $nome ){
        $this->nome = $nome;
    }

    public function setCoordenadas( $coordenadas ){
        $this->coordenadas = $coordenadas;
    }
    
    public function getDescricao()
    {
        return $this->nome . " (" . $this->coordenadas . ")";
    }
    
    public function triggerBeforeSave()
    {
        $this->coordenadas = trim($this->coordenadas);
        
        if( !$this->nome ){
            $this->raise("O nome/descrição da coordenada é obrigatório");
        }
        if( !$this->coordenadas ){
            $this->raise("As coordenadas são obrigatórias");
        }
        
        $tmp = explode(" ", $this->coordenadas);
        if( count($tmp) !== 3 ){
            $this->raise("As coordenadas informadas são inválidas");
        }
        foreach( $tmp as $valor ){
            if( !is_numeric($valor) ){
                $this->raise("A coordenada \"{$valor}\" não é válida");
            }
        }
        
        if( $this->daoAction === "I" ){
            $lst = TeletransporteModel::getList(array(
                "dao.usuario_id = ?" => $this->usuarioId
            ));
            if( $lst['cont_total'] > 1 ){
                $this->raise("É permitido somente 3 pontos de teletransporte por usuário");
            }
        }
        
    }

}