<?php

namespace Model;

class TerrenoModel extends \System\MyModel
{
    protected static $daoTable   = "jogo_terreno";
    protected static $daoPrimary = array('id' => 'id');
    protected static $daoCols    = array(
        'id'                 => 'id',
        'nome'               => 'nome',
        'usuarioId'          => 'usuario_id',
        'coordenadasCentral' => 'coordenadas_central',
        'raio'               => 'raio',
        'altura'             => 'altura',
        'posicaoSaida'       => 'pos_saida',
        'modeloChao'         => 'modelo_chao',
    );
    
    protected $id;
    protected $nome;
    protected $usuarioId;
    protected $coordenadasCentral;
    protected $raio;
    protected $altura;
    protected $posicaoSaida;
    protected $modeloChao;

    public function getId(){
        return $this->id;
    }

    public function getNome(){
        return $this->nome;
    }

    public function getUsuarioId(){
        return $this->usuarioId;
    }

    public function getCoordenadasCentral(){
        return $this->coordenadasCentral;
    }

    public function getRaio(){
        return $this->raio;
    }

    public function getAltura(){
        return $this->altura;
    }

    public function getPosicaoSaida(){
        return $this->posicaoSaida;
    }

    public function getModeloChao(){
        return $this->modeloChao;
    }

    public function setId( $id ){
        $this->id = $id;
    }

    public function setNome( $nome ){
        $this->nome = $nome;
    }

    public function setUsuarioId( $usuarioId ){
        $this->usuarioId = $usuarioId;
    }

    public function setCoordenadasCentral( $coordenadasCentral ){
        $this->coordenadasCentral = $coordenadasCentral;
    }

    public function setRaio( $raio ){
        $this->raio = $raio;
    }

    public function setAltura( $altura ){
        $this->altura = $altura;
    }

    public function setPosicaoSaida( $posicaoSaida ){
        $this->posicaoSaida = $posicaoSaida;
    }

    public function setModeloChao( $modeloChao ){
        $this->modeloChao = $modeloChao;
    }

    public function getDescricao()
    {
        return $this->nome;
    }
    
    public function getUsuarioObj()
    {
        return new UsuarioModel($this->usuarioId);
    }
    
    public function triggerBeforeSave()
    {
        if( $this->daoAction === "I" ){
            
            $lst = TerrenoModel::getList(array(
                "dao.usuario_id = ?" => $this->usuarioId
            ));
            if( $lst['cont_total'] > 1 ){
                $this->raise("Você só pode possuir dois terrenos até o momento");
            }
            
            $this->construirTerreno();
        }elseif( $this->daoAction === "D" ){
//            $this->raise("Não é possível excluir um terreno");
            $this->excluirTerreno();
        }else{
            if( $this->coordenadasCentral !== $this->old->coordenadasCentral ){
                $this->coordenadasCentral = $this->old->coordenadasCentral;
            }
            if( $this->posicaoSaida && $this->posicaoSaida !== $this->old->posicaoSaida ){
//                $this->posicaoSaida = $this->old->posicaoSaida;
            }
        }
        if( $this->posicaoSaida && $this->posicaoSaida !== $this->old->posicaoSaida ){
//            $this->limparSaida();
        }
    }
    
    private function construirTerreno()
    {
        $tmp = explode(" ", $this->coordenadasCentral);
        
        $tx = explode(".", $tmp[0]);
        $x = (Integer) $tx[0];
        $ty = ceil($tmp[1]);
        $y = (Integer) $ty;
        $tz = explode(".", $tmp[2]);
        $z = (Integer) $tz[0];
        
        # PREENCHE O FUNDO
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y - 2;
        $y2 = $y - 2;
        $z1 = $z - $this->raio;
        $z2 = $z + $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
        RconCommand::send($cmd);
        
        # AR - LIMPA O ESPAÇO
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y;
        $y2 = $y + $this->altura;
        $z1 = $z - $this->raio;
        $z2 = $z + $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
        RconCommand::send($cmd);

        # CHAO 2 (cobblestone)
//        $x1 = $x - $this->raio;
//        $x2 = $x + $this->raio;
//        $y1 = $y - 1;
//        $y2 = $y - 1;
//        $z1 = $z - $this->raio;
//        $z2 = $z + $this->raio;
//        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} stone";
//        RconCommand::send($cmd);

        # parede X 1
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y - 2;
        $y2 = $y + $this->altura;
        $z1 = $z + $this->raio;
        $z2 = $z + $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
        RconCommand::send($cmd);
        $y1 = $y2 = $y - 1;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
        RconCommand::send($cmd);


        # parede X 2
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y - 2;
        $y2 = $y + $this->altura;
        $z1 = $z - $this->raio;
        $z2 = $z - $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
        RconCommand::send($cmd);
        $y1 = $y2 = $y - 1;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
        RconCommand::send($cmd);

        # parede Y 1
        $x1 = $x + $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y - 2;
        $y2 = $y + $this->altura;
        $z1 = $z + $this->raio;
        $z2 = $z - $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
        RconCommand::send($cmd);
        $y1 = $y2 = $y - 1;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
        RconCommand::send($cmd);

        # parede Y 2
        $x1 = $x - $this->raio;
        $x2 = $x - $this->raio;
        $y1 = $y - 2;
        $y2 = $y + $this->altura;
        $z1 = $z + $this->raio;
        $z2 = $z - $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
        RconCommand::send($cmd);
        $y1 = $y2 = $y - 1;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
        RconCommand::send($cmd);

        # TETO
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y + $this->altura;
        $y2 = $y + $this->altura;
        $z1 = $z - $this->raio;
        $z2 = $z + $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
        RconCommand::send($cmd);


        # ORIENTAÇÃO / LÃS 5, 1, 11, 14
        $y1 = $y2 = $y;

        # Verde / z+
        $x1 = $x2 = $x;
        $z1 = $z2 = $z + $this->raio - 1;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} wool 5";
        RconCommand::send($cmd);

        # Laranja / z-
        $x1 = $x2 = $x;
        $z1 = $z2 = $z - $this->raio + 1;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} wool 1";
        RconCommand::send($cmd);

        # Azul / x+
        $x1 = $x2 = $x + $this->raio - 1;
        $z1 = $z2 = $z;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} wool 11";
        RconCommand::send($cmd);

        # Vermelho / x-
        $x1 = $x2 = $x - $this->raio + 1;
        $z1 = $z2 = $z;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} wool 14";
        RconCommand::send($cmd);
    }
    
    private function excluirTerreno()
    {
        $tmp = explode(" ", $this->coordenadasCentral);
        
        $tx = explode(".", $tmp[0]);
        $x = (Integer) $tx[0];
        $ty = ceil($tmp[1]);
        $y = (Integer) $ty;
        $tz = explode(".", $tmp[2]);
        $z = (Integer) $tz[0];
        
        # PREENCHE O FUNDO
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y - 2;
        $y2 = $y - 2;
        $z1 = $z - $this->raio;
        $z2 = $z + $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
        RconCommand::send($cmd);
        
        # parede X 1
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y - 2;
        $y2 = $y + $this->altura;
        $z1 = $z + $this->raio;
        $z2 = $z + $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
        RconCommand::send($cmd);

        # parede X 2
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y - 2;
        $y2 = $y + $this->altura;
        $z1 = $z - $this->raio;
        $z2 = $z - $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
        RconCommand::send($cmd);

        # parede Y 1
        $x1 = $x + $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y - 2;
        $y2 = $y + $this->altura;
        $z1 = $z + $this->raio;
        $z2 = $z - $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
        RconCommand::send($cmd);

        # parede Y 2
        $x1 = $x - $this->raio;
        $x2 = $x - $this->raio;
        $y1 = $y - 2;
        $y2 = $y + $this->altura;
        $z1 = $z + $this->raio;
        $z2 = $z - $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
        RconCommand::send($cmd);

        # TETO
        $x1 = $x - $this->raio;
        $x2 = $x + $this->raio;
        $y1 = $y + $this->altura;
        $y2 = $y + $this->altura;
        $z1 = $z - $this->raio;
        $z2 = $z + $this->raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
        RconCommand::send($cmd);
    }
        
    public function getCoordenadaSaida()
    {
        $tmp = explode(" ", $this->coordenadasCentral);
        $tx = explode(".", $tmp[0]);
        $x = (Integer) $tx[0];
        $y = (Integer) ceil($tmp[1]);
        $tz = explode(".", $tmp[2]);
        $z = (Integer) $tz[0];
        
        switch( $this->posicaoSaida ){
            case 'z+': # Verde
                $z = $z + $this->raio + 1;
                break;
            case 'z-': # Laranja
                $z = $z - $this->raio - 1;
                break;
            case 'x+': # Azul
                $x = $x + $this->raio + 1;
                break;
            case 'x-': # Vermelho
                $x = $x - $this->raio - 1;
                break;
        }
        
        return "{$x} {$y} {$z}";
    }
        
    public function getCoordenadaEntrada()
    {
        $tmp = explode(" ", $this->coordenadasCentral);
        $tx = explode(".", $tmp[0]);
        $x = (Integer) $tx[0];
        $y = (Integer) ceil($tmp[1]);
        $tz = explode(".", $tmp[2]);
        $z = (Integer) $tz[0];
        
        switch( $this->posicaoSaida ){
            case 'z+': # Verde
                $z = $z + $this->raio - 1;
                break;
            case 'z-': # Laranja
                $z = $z - $this->raio + 1;
                break;
            case 'x+': # Azul
                $x = $x + $this->raio - 1;
                break;
            case 'x-': # Vermelho
                $x = $x - $this->raio + 1;
                break;
        }
        
        return "{$x} {$y} {$z}";
    }
    
    public function permiteEntrar( $usuario )
    {
        $Player = new PlayerModel(UsuarioModel::getOnline()->getLogin() );
        $x_p = (Float) number_format($Player->position['x']);
        $y_p = (Float) number_format($Player->position['y']);
        $z_p = (Float) number_format($Player->position['z']);
        
        $tmp_coo_sai = explode(" ", $this->getCoordenadaSaida());
        $x_s = (Float) $tmp_coo_sai[0];
        $y_s = (Float) $tmp_coo_sai[1];
        $z_s = (Float) $tmp_coo_sai[2];
        
        debug($x_p, 1);
        debug($x_s, 1);
        debug($z_p, 1);
        debug($z_s, 1);
        
        if( $x_p > $x_s + 2 
            || $x_p < $x_s - 2
            || $z_p > $z_s + 2
            || $z_p < $z_s - 2 ){
            $this->daoErrorMessage = "Você deve estar próximo a entrada da casa";
            return false;
        }
        
        return true;
    }
    
    public function permiteSair( $usuario )
    {
        $Player = new PlayerModel( UsuarioModel::getOnline()->getLogin() );
        $x_p = $Player->position['x'];
        $y_p = $Player->position['y'];
        $z_p = $Player->position['z'];
        
        $tmp_coo_sai = explode(" ", $this->getCoordenadaEntrada());
        $x_s = $tmp_coo_sai[0];
        $y_s = $tmp_coo_sai[1];
        $z_s = $tmp_coo_sai[2];
        
        if( $x_p > $x_s + 2 
            || $x_p < $x_s - 2
            || $z_p > $z_s + 2
            || $z_p < $z_s - 2 ){
            $this->daoErrorMessage = "Você deve estar próximo a entrada da casa";
            return false;
        }
        
        return true;
    }
    
}