<?php

namespace Model;

class ParamModel extends \System\MyModel
{
    protected static $daoTable   = "param";
    protected static $daoPrimary = array('chave' => 'chave');
    protected static $daoCols    = array(
        'chave'    => 'chave',
        'valor' => 'valor',
    );
    
    protected $chave;
    protected $valor;
    
    public function getChave(){
        return $this->chave;
    }

    public function getValor(){
        return $this->valor;
    }

    public function setChave( $chave ){
        $this->chave = $chave;
    }

    public function setValor( $valor ){
        $this->valor = $valor;
    }
    
    public function triggerBeforeSave()
    {
    }

    public function triggerAfterSave()
    {
    }

    public static function get( $chave )
    {
        $Param = new ParamModel($chave);
        return $Param->getValor();
    }

    public static function set( $chave, $valor )
    {
        $Param = new ParamModel($chave);
        $Param->setChave($chave);
        $Param->setValor($valor);
        return $Param->save();
    }
}