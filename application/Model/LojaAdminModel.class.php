<?php

namespace Model;

use Model\LojaAdminHistoricoModel as Historico;

class LojaAdminModel extends \System\MyModel
{
    protected static $daoTable   = "loja_admin";
    protected static $daoPrimary = array('id' => 'id');
    protected static $daoCols    = array(
        'id'                    => 'id',
        'itemCodigo'            => 'item_codigo',
        'operacao'              => 'operacao',
        'valorUnidade'          => 'valor_unidade',
        'quantidadeDisponivel'  => 'quantidade_disponivel',
        'quantidadeMinimaVenda' => 'quantidade_minima_venda',
    );
    
    protected $id;
    protected $itemCodigo;
    protected $operacao;
    protected $valorUnidade;
    protected $quantidadeDisponivel;
    protected $quantidadeMinimaVenda;
    
    public function getId(){
        return $this->id;
    }

    public function getItemCodigo(){
        return $this->itemCodigo;
    }

    public function getOperacao(){
        return $this->operacao;
    }

    public function getValorUnidade(){
        return str_replace(",", "", number_format($this->valorUnidade, 2));
    }

    public function getQuantidadeDisponivel(){
        return (Integer) $this->quantidadeDisponivel;
    }

    public function getQuantidadeMinimaVenda(){
        return (Integer) $this->quantidadeMinimaVenda;
    }

    public function setId( $id ){
        $this->id = $id;
    }

    public function setItemCodigo( $itemCodigo ){
        $this->itemCodigo = $itemCodigo;
    }

    public function setOperacao( $operacao ){
        $this->operacao = $operacao;
    }

    public function setValorUnidade( $valorUnidade ){
        $this->valorUnidade = $valorUnidade;
    }

    public function setQuantidadeDisponivel( $quantidadeDisponivel ){
        $this->quantidadeDisponivel = $quantidadeDisponivel;
    }

    public function setQuantidadeMinimaVenda( $quantidadeMinimaVenda ){
        $this->quantidadeMinimaVenda = $quantidadeMinimaVenda;
    }
    
    public function getItemObj()
    {
        return new ItemModel($this->itemCodigo);
    }
    
    public function getDescricao()
    {
        return ($this->operacao == "V" ? "Venda" : "Compra") .
                " de " . $this->getQuantidadeDisponivel() . 
                " " . $this->getItemCodigo() . "(s)" .
                " em pacotes de " . $this->getQuantidadeMinimaVenda() .
                " por " . $this->getValorUnidade() . " a unidade";
    }
    
    public function triggerBeforeSave()
    {
        if( $this->daoAction === "U" )
        {
            if( $this->quantidadeDisponivel <> $this->old->quantidadeDisponivel )
            {
                if( UsuarioModel::getOnline()->getTipo() !== "A" )
                {
                    $Historico = new Historico();
                    $Historico->setLojaAdminId( $this->id );
                    $Historico->setData( date("Y-m-d H:i:s") );
                    $Historico->setQuantidade( (Integer) $this->old->quantidadeDisponivel - (Integer) $this->quantidadeDisponivel );
                    $Historico->setUsuarioId( UsuarioModel::getOnline()->getId() );
                    $Historico->save();
                }
            }
        }
    }

    public function triggerAfterSave()
    {
        
    }

}