<?php

namespace Model;

class TransacaoModel extends \System\MyModel
{
    protected static $daoTable   = "loja_transacao";
    protected static $daoPrimary = array('id' => 'id');
    protected static $daoCols    = array(
        'id'            => 'id',
        'usuarioId'     => 'usuario_id',
        'itemCodigo'    => 'item_codigo',
        'quantidade'    => 'quantidade',
        'valorUnidade'  => 'valor_unidade',
        'valorTotal'    => 'valor_total',
        'data'          => 'data',
        'operacao'      => 'operacao',
        'descricao'     => 'descricao',
    );
    
    protected $id;
    protected $usuarioId;
    protected $itemCodigo;
    protected $quantidade;
    protected $valorUnidade;
    protected $valorTotal;
    protected $data;
    protected $operacao;
    protected $descricao;
    
    public function getId(){
        return $this->id;
    }

    public function getUsuarioId(){
        return $this->usuarioId;
    }

    public function getItemCodigo(){
        return $this->itemCodigo;
    }

    public function getQuantidade(){
        return $this->quantidade;
    }

    public function getValorUnidade(){
        return $this->valorUnidade;
    }

    public function getValorTotal(){
        return $this->valorTotal;
    }

    public function getData(){
        return $this->data;
    }

    public function getOperacao(){
        return $this->operacao;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function setId( $id ){
        $this->id = $id;
    }

    public function setUsuarioId( $usuarioId ){
        $this->usuarioId = $usuarioId;
    }

    public function setItemCodigo( $itemCodigo ){
        $this->itemCodigo = $itemCodigo;
    }

    public function setQuantidade( $quantidade ){
        $this->quantidade = $quantidade;
    }

    public function setValorUnidade( $valorUnidade ){
        $this->valorUnidade = $valorUnidade;
    }

    public function setValorTotal( $valorTotal ){
        $this->valorTotal = $valorTotal;
    }

    public function setData( $data ){
        $this->data = $data;
    }

    public function setOperacao( $operacao ){
        $this->operacao = $operacao;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
    }

    public function triggerBeforeSave()
    {
        $this->setData(date("Y-m-d H:i:s"));
        
        $this->setValorTotal( (Float) $this->getValorUnidade() * (Integer) $this->getQuantidade() );
        
        $saldo = self::getSaldoUsuario($this->usuarioId);
        
        if( $this->operacao === "C" ){
            if( (Float) $saldo < (Float) $this->getValorTotal() ){
                $this->raise("Saldo insuficiente");
            }
            
            self::rconGiveItem($this->usuarioId, $this->quantidade, $this->itemCodigo);
            
        }elseif( $this->itemCodigo ){
             
            $qtdeInventario = (Integer) self::rconCountItem($this->usuarioId, $this->itemCodigo);
            
            if( $qtdeInventario < $this->quantidade ){
                $this->raise("Itens insuficientes no inventário do jogador");
            }
            
            self::rconDeleteItem($this->usuarioId, $this->quantidade, $this->itemCodigo);
        }
    }

    public function triggerAfterSave()
    {
        
    }

    public static function getSaldoUsuario( $usuario )
    {
        $usuario_id = is_object($usuario) ? $usuario->getId() : $usuario;
        
        $resultCompra = TransacaoModel::getList(array(
            "dao.operacao = 'C'",
            "dao.usuario_id = ?" => $usuario_id
        ), array("sum(dao.valor_total) as valorTotal"), 0, 1);
        
        $resultVenda = TransacaoModel::getList(array(
            "dao.operacao = 'V'",
            "dao.usuario_id = ?" => $usuario_id
        ), array("sum(dao.valor_total) as valorTotal"), 0, 1);
        
        $valorCompra = str_replace(",", "", number_format($resultCompra['rows'][0]->getValorTotal(), 2));
        $valorVenda = str_replace(",", "", number_format($resultVenda['rows'][0]->getValorTotal(), 2));
        
        return str_replace(",", "", number_format( $valorVenda - $valorCompra, 2 ));
    }
    
    # Give amount item to Player
    private static function rconGiveItem( $usuario, $amount, $itemcode )
    {
        $Usuario = is_object($usuario) ? $usuario : new UsuarioModel($usuario);
        $usename = $Usuario->getLogin();
        
        $tmp = explode(":", $itemcode);
        $t_code = $tmp[0];
        $t_data = empty($tmp[1]) ? "" : " {$tmp[1]}";
        
        $cmd = "give {$usename} {$t_code} " . (String) $amount . "{$t_data}";
        
        $response = trim(RconCommand::send($cmd));
        
        if( stristr($response, "no items to remove") ){
            self::$mensagem = "Nenhum item disponível no inventário";
            return false;
        }elseif( stristr($response, "That player cannot be found") ){
            self::$mensagem = "O Jogador deve estar online";
            return false;
        }
        return true;
    }
    
    # Count amount item from Player's Inventory
    private static function rconCountItem( $usuario, $itemcode )
    {
        $Usuario = is_object($usuario) ? $usuario : new UsuarioModel($usuario);
        $usename = $Usuario->getLogin();
        
        $tmp = explode(":", $itemcode);
        $t_code = $tmp[0];
        $t_data = empty($tmp[1]) ? "0" : $tmp[1];
        
        $cmd = "clear {$usename} {$t_code} {$t_data}";
        
        $response = trim(RconCommand::send($cmd));
        
        if( stristr($response, "no items to remove") )
        {
            return 0;
        }
        
        if( stristr($response, "Cleared the inventory of") )
        {
            $exp = explode(" ", $response);
            
            unset( $exp[ count($exp)-1 ] );
            
            $amount = (Integer) end($exp);
            
            $amount_to_add = $amount;
            
            while( $amount_to_add > 64 )
            {
                self::rconGiveItem( $usuario, 64, $itemcode );
                $amount_to_add = $amount_to_add - 64;
            }
            self::rconGiveItem( $usuario, $amount_to_add, $itemcode );
            
            return $amount;
        }else{
            return 0;
        }
    }
    
    # Delete amount item from Player's Inventory
    private static function rconDeleteItem( $usuario, $amount, $itemcode )
    {
        $Usuario = is_object($usuario) ? $usuario : new UsuarioModel($usuario);
        $usename = $Usuario->getLogin();
        
        $tmp = explode(":", $itemcode);
        $t_code = $tmp[0];
        $t_data = empty($tmp[1]) ? "0" : $tmp[1];
        
        $cmd = "clear {$usename} {$t_code} {$t_data} " . (String) $amount;
        
        $response = trim(RconCommand::send($cmd));
        
        if( stristr($response, "no items to remove") )
        {
            self::$mensagem = "Os itens necessário não constam no seu inventário";
            return false;
        }
        
        if( stristr($response, "Cleared the inventory of") )
        {
            $exp = explode(" ", $response);
            
            unset( $exp[ count($exp)-1 ] );
            
            $amount_rest = (Integer) end($exp);
            
            if( $amount_rest < $amount )
            {
                self::rconGiveItem($usuario, $amount_rest, $itemcode);
                self::$mensagem = "Quantidade de itens insuficiente no seu inventário";
                return false;
            }
            
        }else{
            return false;
        }
    }
}