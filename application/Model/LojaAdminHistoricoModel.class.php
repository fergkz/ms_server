<?php

namespace Model;

class LojaAdminHistoricoModel extends \System\MyModel
{
    protected static $daoTable   = "loja_admin_historico";
    protected static $daoPrimary = array('id' => 'id');
    protected static $daoCols    = array(
        'id'            => 'id',
        'lojaAdminId'   => 'loja_admin_id',
        'usuarioId'     => 'usuario_id',
        'quantidade'    => 'quantidade',
        'data'          => 'data',
    );
    
    protected $id;
    protected $lojaAdminId;
    protected $usuarioId;
    protected $quantidade;
    protected $data;
    
    public function getId(){
        return $this->id;
    }

    public function getLojaAdminId(){
        return $this->lojaAdminId;
    }

    public function getUsuarioId(){
        return $this->usuarioId;
    }

    public function getQuantidade(){
        return $this->quantidade;
    }

    public function getData(){
        return $this->data;
    }

    public function setId( $id ){
        $this->id = $id;
    }

    public function setLojaAdminId( $lojaAdminId ){
        $this->lojaAdminId = $lojaAdminId;
    }

    public function setUsuarioId( $usuarioId ){
        $this->usuarioId = $usuarioId;
    }

    public function setQuantidade( $quantidade ){
        $this->quantidade = $quantidade;
    }

    public function setData( $data ){
        $this->data = $data;
    }
    
    public function triggerBeforeSave()
    {
        
    }

    public function triggerAfterSave()
    {
        
    }

}