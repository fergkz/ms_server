<?php

namespace Model;

include_once(path."/application/Model/PHP-NBT-Decoder-Encoder/nbt.class.php");
//die(path."/application/Model/PHP-NBT-Decoder-Encoder/nbt.class.php");

class MinecraftPlayerNBT extends \NBT {

	public $properties = array();
	public $debug = 0;
    public $filename = "";

	public function __construct($filename='') {
        $this->filename = $filename;
		if(is_file($this->filename)) {
			\NBT::loadFile($this->filename);
			$this->properties = &$this->root[0]['value'];
		}
	}

	public function getProperty($property) {
		if($this->debug > 0) echo "Looking for '$property'...\n";
		foreach( $this->properties as $row ){
            if( $row['name'] === $property ){
                return $row['value'];
            }
        }
	}
    
    public function salva()
    {
        $this->writeFile($this->filename);
    }
    
}