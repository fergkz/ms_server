<?php

namespace Model;

class ItemModel extends \System\MyModel
{
    protected static $daoTable   = "jogo_item";
    protected static $daoPrimary = array('codigo' => 'codigo');
    protected static $daoCols    = array(
        'codigo'    => 'codigo',
        'descricao' => 'descricao',
        'imagemUrl' => 'img_url',
    );
    
    protected $codigo;
    protected $descricao;
    protected $imagemUrl;
    
    public function getCodigo(){
        return $this->codigo;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function getImagemUrl(){
        return $this->imagemUrl;
    }

    public function setCodigo( $codigo ){
        $this->codigo = $codigo;
    }

    public function setDescricao( $descricao ){
        $this->descricao = $descricao;
    }

    public function setImagemUrl( $imagemUrl ){
        $this->imagemUrl = $imagemUrl;
    }
    
    public function triggerBeforeSave()
    {
        if( !$this->imagemUrl ){
            $this->imagemUrl = "http://www.minecraftopia.com/images/blocks/{$this->codigo}.png";
        }
    }

    public function triggerAfterSave()
    {
        
    }

}