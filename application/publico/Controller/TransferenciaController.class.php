<?php

use Model\UsuarioModel as UsuarioModel;
use Model\Inventario as Inventario;

class TransferenciaController extends \System\MyController
{
    public function venderAction( $codigo_item, $quantidade )
    {
        $playername = $this->post("player");
        
        $Player = new UsuarioModel($playername);
        debug($Player);
        
        $Inventario = new Inventario($Player);

        switch( $codigo_item ){
            case "cobblestone":
                $quantidade = 64;

                $qtde_inventario = $Inventario->count("cobblestone");

                if( $qtde_inventario < $quantidade ){
                    $this->flash("mensagem", "Você deve pagar no mínimo {$quantidade} cobblestones por 1 culhão");
                }else{
                    $Inventario->delete($quantidade, "cobblestone");
                    $Inventario->give(1, "diamond");

                    $this->flash("mensagem", "Troca efetuada com sucesso");
                }
                break;
        }

        $this->redirect("");
    }
    
}