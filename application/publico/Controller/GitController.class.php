<?php

use MVC\Controller as Controller;

class GitController extends Controller
{
    public function pullAction()
    {
        $command[] = 'sudo git pull';
        
        echo "<div style='font-family:courier;font-size:10px;'><fieldset style='margin:4px 0;'>START</fieldset>";
        
        foreach( $command as $cmd ){
            try{
                $return = exec($cmd);
                echo "<fieldset style='margin:4px 0;'>
                        <legend style='white-space:nowrap;background-color:#000;color:#FFF;padding:4px 6px;'>{$cmd}</legend>
                        ";debug($return,1); echo"
                      </fieldset>";
            }catch( Exception $e ){
                debug($e, 0, 'Erro');
            }
        }
        echo "<fieldset style='margin:4px 0;'>END</fieldset>";
        
        echo "<br/><br/>";
        echo "<div style='font-size: 14px'>";
        echo "Adicionar ao arquivo \"/etc/sudoers\" a seguinte permissão: <br/>";
        echo "<i>".exec('whoami')." ALL = NOPASSWD: /usr/bin/git</i><br/>";
        echo "Após isso, reiniciar o apache com /etc/init.d/apache2 restart";
        echo "</div>";
    }
}