<?php

ini_set('display_errors', 0);

use Model\UsuarioModel as Usuario;
$GLOBALS['page']['UsuarioOnline'] = Usuario::getOnline();

if( !Usuario::getOnline() 
    && !stristr($GLOBALS['url']['request'], "sessao") 
    && !stristr($GLOBALS['url']['request'], "git") 
    && !stristr($GLOBALS['url']['request'], "cron") 
){
    $Controller = new System\MyController();
    $Controller->redirect("painel/sessao/login");
}

//if( !Usuario::getOnline() ){
//    
//    $GLOBALS['request']['mode'] = "f";
//    $GLOBALS['request']['module'] = "sessao";
//    $GLOBALS['request']['submodule'] = "login";
//    
//    _setError("Você deve estar logado para acessar o painel de controle");
//    
//}else{

//    if( Projeto::getSelecionado() ){
//        $GLOBALS['page']['projeto_atual'] = Projeto::getSelecionado();
//    }
//    $GLOBALS['page']['lista_projetos'] = Projeto::getListUsuarioOnline();

//}