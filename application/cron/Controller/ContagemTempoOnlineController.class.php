<?php

use Model\RconCommand as RconCommand;
use Model\ParamModel as Param;
use Model\TransacaoModel as TransacaoModel;
use Model\UsuarioModel as UsuarioModel;
use Model\MinecraftPlayerNBT as MinecraftPlayer;
use Model\PlayerModel as Player;

class ContagemTempoOnlineController extends System\MyController
{   
    public function indexAction()
    {
        $list = RconCommand::send("list");
        
        $tmp = explode("online:", $list);
        
        $usuarios = explode(",", trim($tmp[1]));
        
        $valorPorMinuto = (Float) Param::get("money_minuto");
        $textoDescricao = "Tempo de jogo (1 minuto)";
        
        foreach( $usuarios as $usuario_login ){
            
            $lst = UsuarioModel::getList(array(
                "dao.login = ?" => trim($usuario_login)
            ));
            
            if( $lst['cont_total'] > 0 ){
                $Usuario = $lst['rows'][0];
                
                $tmp = TransacaoModel::getList(array(
                    "dao.valor_total = ?" => $valorPorMinuto,
                    "dao.valor_unitario = ?" => $valorPorMinuto,
                    "dao.item_codigo is null",
                    "trim(upper(dao.descricao)) = trim(upper(?))" => $textoDescricao
                ), null, 0, 1);
                
                if( $tmp['cont_total'] > 0 ){
                    $Transacao = $tmp['rows'][0];
                }else{
                    $Transacao = new TransacaoModel();
                }
                
                $Transacao->setDescricao( $textoDescricao );
                $Transacao->setData( date("Y-m-d H:i:s") );
                $Transacao->setItemCodigo(null);
                $Transacao->setOperacao("V");
                $Transacao->setQuantidade( 1 );
                $Transacao->setUsuarioId( $Usuario->getId() );
                $Transacao->setValorTotal( $valorPorMinuto );
                $Transacao->setValorUnidade( $valorPorMinuto );
                
                $Transacao->save();
//                debug($Transacao, 1);
            }
            
        }
    }
    
    public function loadPlayerAction($playername)
    {
        $Player = new Player($playername);

        echo '<meta http-equiv="refresh" content="1">';

        debug($Player, 1);
    }
    
    public function detalheAction()
    {
        $path = $GLOBALS['config']['minecraft']['server_path'];
        
//        $nbt = new \Model\MinecraftPlayerNBT($path.'world/level.dat');
//        debug($nbt->properties);
        
//        $nbt = new \Model\MinecraftPlayerNBT($path.'world/level.dat');
//        debug($nbt->properties);
        
        
//        $jogadores = UsuarioModel::getList();
//        
//        $usuarios_exist = array();
//        foreach( $jogadores['rows'] as $Usuario ){
//            $usuarios_exist[] = $Usuario->getLogin();
//        }
        
        $path_playerdata = $path.'world'.ds.'playerdata';
        RconCommand::send("save-all");
        foreach( json_decode(file_get_contents($path.'usercache.json')) as $sv_jog ){
//            echo '<meta http-equiv="refresh" content="1">';
//            $nbt = new \Model\MinecraftPlayerNBT($path_playerdata.ds.$sv_jog->uuid.".dat");
//            $nbt->properties[29]['value']['value'][0] = -162;
//            $nbt->properties[29]['value']['value'][1] = 71;
//            $nbt->properties[29]['value']['value'][2] = 646;
////            $nbt->salva();
//            debug($nbt->properties[29]['value']['value'][0]);
            
            
            $nbt = new \Model\MinecraftPlayerNBT($path_playerdata.ds.$sv_jog->uuid.".dat");
            
            if( $sv_jog->name !== "Fernando"){
                continue;
            }
            
            $sv_jogadores[] = array(
                'name' => $sv_jog->name,
                'uuid' => $sv_jog->uuid,
                'posicionamento' => $nbt->getProperty("Pos"),
                'data' => $nbt->properties
            );
            
            ####################
            ####################
            ####################
            ####################
            ####################
            ####################
            ####################
            # /fill <x1> <y1> <z1> <x2> <y2> <z2> <TileName> [dataValue] [oldBlockHandling] [dataTag]

            $pos = $nbt->getProperty("Pos");
            $x = $pos['value'][0];
            $y = $pos['value'][1];
            $z = $pos['value'][2];
//            $x = -2512;
//            $y = 63;
//            $z = 5351;
            
//            $x += 6;
//            $cmd = "tp Fernando {$x} {$y} {$z}";
//            debug(RconCommand::send($cmd));
            
            
//            debug($pos['value']);
            
            # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            $x1 = $x - 10;
            $x2 = $x + 10;
            $y1 = $y - 1;
            $y2 = $y + 19;
            $z1 = $z - 10;
            $z2 = $z + 10;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
            debug(RconCommand::send($cmd), 1);
            $x1 = $x - 10;
            $x2 = $x + 10;
            $y1 = $y - 3;
            $y2 = $y - 1;
            $z1 = $z - 10;
            $z2 = $z + 10;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} stone";
            debug(RconCommand::send($cmd));
            
            # CHAO
            $x1 = $x - 5;
            $x2 = $x + 5;
            $y1 = $y - 2;
            $y2 = $y - 2;
            $z1 = $z - 5;
            $z2 = $z + 5;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
            debug(RconCommand::send($cmd), 1);
            
            # AR
            $x1 = $x - 10;
            $x2 = $x + 10;
            $y1 = $y - 1;
            $y2 = $y + 30;
            $z1 = $z - 10;
            $z2 = $z + 10;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
            debug(RconCommand::send($cmd), 1);
            
            # CHAO 2 (cobblestone)
            $x1 = $x - 5;
            $x2 = $x + 5;
            $y1 = $y - 1;
            $y2 = $y - 1;
            $z1 = $z - 5;
            $z2 = $z + 5;
//            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} command_block 1 replace {Command:\"say ola\"}";
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} stone";
            debug(RconCommand::send($cmd), 1);
            
            # parede X 1
            $x1 = $x - 5;
            $x2 = $x + 5;
            $y1 = $y - 2;
            $y2 = $y + 20;
            $z1 = $z + 5;
            $z2 = $z + 5;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
            debug(RconCommand::send($cmd), 1);
            $y1 = $y2 = $y - 1;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
            debug(RconCommand::send($cmd), 1);
            
            
            # parede X 2
            $x1 = $x - 5;
            $x2 = $x + 5;
            $y1 = $y - 2;
            $y2 = $y + 20;
            $z1 = $z - 5;
            $z2 = $z - 5;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
            debug(RconCommand::send($cmd), 1);
            $y1 = $y2 = $y - 1;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
            debug(RconCommand::send($cmd), 1);
            
            # parede Y 1
            $x1 = $x + 5;
            $x2 = $x + 5;
            $y1 = $y - 2;
            $y2 = $y + 20;
            $z1 = $z + 5;
            $z2 = $z - 5;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
            debug(RconCommand::send($cmd), 1);
            $y1 = $y2 = $y - 1;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
            debug(RconCommand::send($cmd), 1);
            
            # parede Y 2
            $x1 = $x - 5;
            $x2 = $x - 5;
            $y1 = $y - 2;
            $y2 = $y + 20;
            $z1 = $z + 5;
            $z2 = $z - 5;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
            debug(RconCommand::send($cmd), 1);
            $y1 = $y2 = $y - 1;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} bedrock";
            debug(RconCommand::send($cmd), 1);
            
            # TETO
            $x1 = $x - 5;
            $x2 = $x + 5;
            $y1 = $y + 20;
            $y2 = $y + 20;
            $z1 = $z - 5;
            $z2 = $z + 5;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} barrier";
            debug(RconCommand::send($cmd), 1);
            
            
            # ORIENTAÇÃO / LÃS 5, 1, 11, 14
            $y1 = $y2 = $y;
            
            # VERDE
            $x1 = $x2 = $x;
            $z1 = $z2 = $z + 4;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} wool 5";
            debug(RconCommand::send($cmd), 1);
            
            # LARANJA
            $x1 = $x2 = $x;
            $z1 = $z2 = $z - 4;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} wool 1";
            debug(RconCommand::send($cmd), 1);
            
            # AZUL
            $x1 = $x2 = $x + 4;
            $z1 = $z2 = $z;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} wool 11";
            debug(RconCommand::send($cmd), 1);
            
            # VERMELHO
            $x1 = $x2 = $x - 4;
            $z1 = $z2 = $z;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} wool 14";
            debug(RconCommand::send($cmd), 1);
            
            
            
            debug( $nbt->getProperty("Pos") );
            
        }
        
        
//        scandir($path_palyerdata)
        
//        $path_playerdata = $path.'world'.ds.'playerdata';
        
//        debug(scandir($path_palyerdata));
        
        foreach( scandir($path_playerdata) as $filename ){
//            if( in_array($filename, array('.','..')) ){
//                continue;
//            }
            $nbt = new \Model\MinecraftPlayerNBT($path_palyerdata.ds.$filename);
            if( $filename == "41a424f0-1252-3241-a1f4-ff5c35dc40c4.dat" ){
//                debug( $nbt->getProperty("SelectedItem") );
//                debug( $nbt->getProperty("Pos") );
                debug( $nbt->properties );
            }
        }
        
        
        debug($usuarios_exist);
        
        
//        phpinfo();exit;
        
        ini_set('display_errors', E_ALL);
        
//        require(path."/application/Model/PHP-NBT-Decoder-Encoder/nbt.class.php");
//        require(path."/application/Model/MinecraftPlayerNBT.class.php");
//        
        $filename = $path.'world'.ds.'playerdata'.ds.'41a424f0-1252-3241-a1f4-ff5c35dc40c4.dat';
        
        $nbt = new \Model\MinecraftPlayerNBT($filename);
//        $res = $nbt->getProperty('Inventory');
//        $itens_dat = $res['value'];
        
//        RconCommand::send("save-all");
        
        echo md5(json_encode( $nbt->properties ));
        
        debug($nbt->properties);
        
        foreach( $itens_dat as $row ){
            
            $itens[] = array(
                'id' => $row[2]['value'],
                'nome' => str_replace("minecraft:", "", $row[0]['value']),
                'qtde' => $row[2]['value']
            );
            
        }
        
        debug($itens);
        
//        class MinecraftPlayerNBT extends \NBT {
//            public $properties = array();
//            public $debug = 0;
//            
//            
//            
//        }
        
        
//        NBT::loadFile($filename);
        
        debug($nbt);
        
        echo ("xx");
        
        
        
        
        
        
//        $conteudo = file_get_contents($path.'usercache.json');
//        debug( json_decode($conteudo) );
        
//        $conteudo = file_get_contents($path.'world/stats/43c76a0d-c44e-3dcf-b944-96d9920f3b71.json');
//        debug( json_decode($conteudo) );
        
    }
    
    public function limpezaAction()
    {
        $path = $GLOBALS['config']['minecraft']['server_path'];
        
        $path_playerdata = $path.'world'.ds.'playerdata';
        RconCommand::send("save-all");
        foreach( json_decode(file_get_contents($path.'usercache.json')) as $sv_jog ){
            
            $nbt = new \Model\MinecraftPlayerNBT($path_playerdata.ds.$sv_jog->uuid.".dat");
            
            if( $sv_jog->name !== "Fernando"){
                continue;
            }
            
            $sv_jogadores[] = array(
                'name' => $sv_jog->name,
                'uuid' => $sv_jog->uuid,
                'posicionamento' => $nbt->getProperty("Pos"),
                'data' => $nbt->properties
            );
            
            $pos = $nbt->getProperty("Pos");
            $x = $pos['value'][0];
            $y = $pos['value'][1];
            $z = $pos['value'][2];
            
            # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            $x1 = $x - 15;
            $x2 = $x + 15;
            $y1 = $y - 1;
            $y2 = $y + 10;
            $z1 = $z - 15;
            $z2 = $z + 15;
            $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
            
            echo "<br/>{$cmd}<br/>";
            
            die("y");
            debug(RconCommand::send($cmd), 1);
        }
    }
    
    
    
}