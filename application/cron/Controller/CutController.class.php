<?php

use Model\RconCommand as RconCommand;
use Model\PlayerModel as Player;

class CutController extends System\MyController
{   
    public function indexAction( $raio = 5, $altura = 10 )
    {
        $Player = new Player("Fernando");
        
        $x = $Player->position['x'];
        $y = $Player->position['y'];
        $z = $Player->position['z'];
        
        $x1 = $x - $raio;
        $x2 = $x + $raio;
        $y1 = $y;
        $y2 = $y + $altura;
        $z1 = $z - $raio;
        $z2 = $z + $raio;
        $cmd = "fill {$x1} {$y1} {$z1} {$x2} {$y2} {$z2} air";
        
        debug(RconCommand::send($cmd), 1);
    }
    
}