<?php

class ServidorController extends System\MyController
{   
    public function mensagemAction()
    {
        if( !empty($_POST) ){
            $subtitulo = $this->post("subtitulo");
            if( $subtitulo ){
                Model\RconCommand::send("title @a subtitle \"{$subtitulo}\"");
            }
            $titulo = $this->post("titulo");
            Model\RconCommand::send("title @a title \"{$titulo}\"");
        }
        
        $this->view("servidor/mensagem.twig");
    }
    
    public function comandoAction()
    {
        $render = array();
        
        if( !empty($_POST) ){
            $comando = $this->post("comando");
            $render['resposta'] = Model\RconCommand::send("{$comando}");
        }
        
        $this->view("servidor/comando.twig", $render);
    }
    
    public function getLogAction()
    {
        $render['conteudo'] = file_get_contents( rtrim($GLOBALS['config']['minecraft']['server_path'],"\\..\/") . "/logs/latest.log" );
        $this->view("servidor/log.twig", $render);
    }
}
