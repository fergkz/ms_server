<?php

use Model\LojaAdminModel as LojaAdminModel;
use Model\ItemModel as ItemModel;

class LojaController extends System\MyController
{   
    public function vendaAction()
    {
        $render['operacao'] = "venda";
        $render['lojas'] = LojaAdminModel::getList(array(
            "dao.operacao = 'V'",
            "dao.quantidade_disponivel <> 0"
        ), null, 0, null, array("dao.quantidade_disponivel desc"));
        $this->view("loja/lista.twig", $render);
    }
    
    public function compraAction()
    {
        $render['operacao'] = "compra";
        $render['lojas'] = LojaAdminModel::getList(array(
            "dao.operacao = 'C'",
            "dao.quantidade_disponivel <> 0"
        ), null, 0, null, array("dao.quantidade_disponivel desc"));
        $this->view("loja/lista.twig", $render);
    }
    
    public function cadastroAction( $operacao, $lojaId = null )
    {
        $render['Loja'] = new LojaAdminModel($lojaId);
        $render['itens'] = ItemModel::getList();
        $render['operacao'] = $operacao;
        $this->view("loja/cadastro.twig", $render);
    }
    
    public function salvarCadastroAction( $operacao, $lojaId = null )
    {
        $Loja = new LojaAdminModel($lojaId);
        
        $Loja->setItemCodigo( $this->post("item") );
        $Loja->setOperacao( $operacao == "venda" ? "V" : "C" );
        $Loja->setQuantidadeDisponivel( $this->post("quantidade_disponivel") );
        $Loja->setQuantidadeMinimaVenda( $this->post("quantidade_minima") );
        $Loja->setValorUnidade( $this->post("valor_unidade") );
        
        if( $Loja->save() ){
            $render['status'] = true;
        }else{
            $render['status'] = false;
            $render['mensagem'] = $Loja->daoErrorMessage;
        }
        
        $this->json($render);
    }
}
