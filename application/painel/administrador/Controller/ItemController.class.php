<?php

use Model\ItemModel as Item;

class ItemController extends System\MyController
{   
    public function listaAction()
    {
        $render['itens'] = Item::getList();
        $this->view("item/lista.twig", $render);
    }
    
    public function cadastroAction( $itemCodigo )
    {
        if( $itemCodigo == "novo" ){
            $itemCodigo = null;
        }
        
        $render["Item"] = new Item($itemCodigo);
        
        $this->view("item/cadastro.twig", $render);
    }
    
    public function salvarCadastroAction( $itemCodigo )
    {
        $Item = new Item($itemCodigo);
        
        if( !$itemCodigo ){
            $Item->setCodigo( $this->post("codigo") );
        }
        
        $Item->setDescricao( $this->post("descricao") );
        $Item->setImagemUrl( $this->post("img_url") );
        
        if( $Item->save() ){
            $render['status'] = true;
        }else{
            $render['status'] = false;
            $render['mensagem'] = $Item->daoErrorMessage;
        }
        
        $this->json($render);
    }
}
