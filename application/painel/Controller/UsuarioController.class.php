<?php

use Model\UsuarioModel as Usuario;

class UsuarioController extends System\MyController
{   
    public function cadastroAction()
    {
        $render = array();
        $this->view("usuario/cadastro.twig");
    }
    
    public function salvarAlteracoesAction()
    {
        $render['status'] = true;
        
        $Usuario = Usuario::getOnline();
        
        $Usuario->setNome( $this->post("nome") );
        $Usuario->setEmail( $this->post("email") );
        
        if( $this->post("senha") ){
            if( $this->post("senha") !== $this->post("rsenha") ){
                $render['status'] = false;
                $render['mensagem'] = "As senhas informadas devem ser iguais";
            }else{
                $Usuario->setSenha( $this->post("senha") );
            }
        }
        
        if( $render['status'] ){
            if( !$Usuario->save() ){
                $render['status'] = false;
                $render['mensagem'] = $Usuario->daoErrorMessage;
            }
        }
        
        $this->json($render);
    }
    
}
