<?php

use Model\UsuarioModel as Usuario;

class SessaoController extends System\MyController
{   
    public function loginAction()
    {
        Usuario::logoff();
        $this->view("sessao/login.twig");
    }
    
    public function entrarAction()
    {
        if( Usuario::login($this->post("login"), $this->post("senha")) ){
            $render['status'] = true;
        }else{
            $render['status'] = false;
            $render['mensagem'] = "Usuário e senha inválidos";
        }
        
        $this->json($render);
    }
    
    public function cadastroAction()
    {
        Usuario::logoff();
        $this->view("sessao/cadastro.twig");
    }
    
    public function cadastrarAction()
    {
        if( Usuario::getExistente($this->post("login"), $this->post("senha")) ){
            $render['status'] = false;
            $render['mensagem'] = "Usuário já existente";
        }else{
            
            $Usuario = new Usuario();
            
            if( $this->post("senha") !== $this->post("rsenha") ){
                $render['status'] = false;
                $render['mensagem'] = "As senhas não conferem";
            }else{
            
                $Usuario->setLogin( $this->post("login") );
                $Usuario->setNome( $this->post("nome") );
                $Usuario->setEmail( $this->post("email") );
                $Usuario->setSenha( $this->post("senha") );
                $Usuario->setStatus("A");
                $Usuario->setTipo("N");
                
                if( $Usuario->save() ){
                    
                    $filename = $GLOBALS['config']['minecraft']['server_path']."whitelist.json";
                    
                    $content = file_get_contents($filename);

                    $content .= "{$Usuario->getLogin()}\n";
                    file_put_contents($filename, $content);
                    
                    $render['status'] = true;
                }else{
                    $render['status'] = false;
                    $render['mensagem'] = $Usuario->daoErrorMessage;
                }
                
            }
        }
        
        $this->json($render);
    }
    
    public function logoffAction()
    {
        Usuario::logoff();
        $this->redirect("painel");
    }
}
