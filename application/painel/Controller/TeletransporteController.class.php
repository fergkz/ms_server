<?php

use Model\UsuarioModel as Usuario;
use Model\TeletransporteModel as Teletransporte;
use Model\ParamModel as Param;
use Model\RconCommand as Rcon;
use Model\TransacaoModel as Transacao;
use Model\PlayerModel as Player;

class TeletransporteController extends System\MyController
{   
    public function listaAction()
    {
        $render['lista'] = Teletransporte::getList(array(
            "dao.usuario_id = ?" => Usuario::getOnline()->getId()
        ));
        $render['valor_teletransporte'] = "$ " . Param::get('valor_teletransporte');
        $this->view("teletransporte/lista.twig", $render);
    }
    
    public function cadastroAction( $teletransporteId = null )
    {
        $Teletransporte = new Teletransporte($teletransporteId);
        
        if( $teletransporteId && $teletransporteId !== "novo" && $Teletransporte->getUsuarioId() !== Usuario::getOnline()->getId() ){
            $this->redirect("painel/teletransporte/cadastro/novo");
        }else{
            $Teletransporte->setUsuarioId( Usuario::getOnline()->getId() );
        }
        
        $render["Teletransporte"] = $Teletransporte;
        $this->view("teletransporte/cadastro.twig", $render);
    }
    
    public function salvarAlteracoesAction( $teletransporteId = null )
    {
        $render['status'] = true;
        
        $Teletransporte = new Teletransporte($teletransporteId);
        if( $teletransporteId && $teletransporteId !== "novo" && $Teletransporte->getUsuarioId() !== Usuario::getOnline()->getId() ){
            $render['mensagem'] = "Operação não permitida para este usuário";
        }else{
            $Teletransporte->setUsuarioId( Usuario::getOnline()->getId() );
        }
        
        $Teletransporte->setNome( $this->post("nome") );
        
        $Player = new Player( Usuario::getOnline()->getLogin() );
        
        $Teletransporte->setCoordenadas("{$Player->position['x']} {$Player->position['y']} {$Player->position['z']}");
//        $Teletransporte->setCoordenadas( $this->post("coordenadas") );
//        debug($Teletransporte);
        
        if( $render['status'] ){
            if( !$Teletransporte->save() ){
                $render['status'] = false;
                $render['mensagem'] = $Teletransporte->daoErrorMessage;
            }
        }
        
        $this->json($render);
    }
    
    public function goToAction( $teletransporteId )
    {
        $render['status'] = false;
        
        $Teletransporte = new Teletransporte($teletransporteId);
        if( $Teletransporte->getUsuarioId() !== Usuario::getOnline()->getId() ){
            $render['mensagem'] = "Operação não permitida para este usuário";
        }else{
            $usuario_nome = Usuario::getOnline()->getLogin();
            $coordenada = $Teletransporte->getCoordenadas();
            $valor = Param::get('valor_teletransporte');
            
            $cmd = "tp {$usuario_nome} {$coordenada}";
            
            $Transacao = new Transacao();
            $Transacao->setData( date("Y-m-d H:i:s") );
            $Transacao->setDescricao("Teletransporte para ".$Teletransporte->getDescricao());
            $Transacao->setItemCodigo( null );
            $Transacao->setQuantidade( 1 );
            $Transacao->setOperacao('C');
            $Transacao->setUsuarioId(  Usuario::getOnline()->getId() );
            $Transacao->setValorTotal( $valor );
            $Transacao->setValorUnidade( $valor );
            
            if( $Transacao->save() ){
                Rcon::send($cmd);
                $render['status'] = true;
                $render['mensagem'] = "Comando de teletransporte enviado";
            }else{
                $render['mensagem'] = $Transacao->daoErrorMessage;
            }
        }
        
        $this->json($render);
    }
}
