<?php

use Model\UsuarioModel as Usuario;
use Model\TerrenoModel as Terreno;
use Model\ParamModel as Param;
use Model\RconCommand as Rcon;
use Model\TransacaoModel as Transacao;
use Model\PlayerModel as Player;

class TerrenoController extends System\MyController
{   
    public function listaAction()
    {
        $render['lista'] = Terreno::getList(array(
            "dao.usuario_id = ?" => Usuario::getOnline()->getId()
        ));
        $render['valor_terreno'] = "$ " . Param::get('valor_terreno');
        $this->view("terreno/lista.twig", $render);
    }
    
    public function cadastroAction( $terrenoId = null )
    {
        $Terreno = new Terreno($terrenoId);
        
        if( $terrenoId && $terrenoId !== "novo" && $Terreno->getUsuarioId() !== Usuario::getOnline()->getId() ){
            $this->redirect("painel/terreno/cadastro/novo");
        }else{
            $Terreno->setUsuarioId( Usuario::getOnline()->getId() );
        }
        
        $render["Terreno"] = $Terreno;
        $render['valor_terreno'] = "$ " . Param::get('valor_terreno');
        
        $this->view("terreno/cadastro.twig", $render);
    }
    
    public function salvarAlteracoesAction( $terrenoId = null )
    {
        $render['status'] = true;
        
        $Terreno = new Terreno($terrenoId);
        if( $terrenoId && $terrenoId !== "novo" && $Terreno->getUsuarioId() !== Usuario::getOnline()->getId() ){
            $render['mensagem'] = "Operação não permitida para este usuário";
        }else{
            $Player = new Player( Usuario::getOnline()->getLogin() );
            $Terreno->setUsuarioId( Usuario::getOnline()->getId() );
            $Terreno->setCoordenadasCentral("{$Player->position['x']} {$Player->position['y']} {$Player->position['z']}");
        }
        
        $Terreno->setNome( $this->post("nome") );
        $Terreno->setAltura(10);
        $Terreno->setModeloChao("2");
        $Terreno->setPosicaoSaida( $this->post('pos_saida') );
        $Terreno->setRaio(5);
        
        if( $render['status'] ){
            
            if( !$terrenoId ){
                $valor = Param::get('valor_terreno');
                
                $Transacao = new Transacao();
                $Transacao->setData( date("Y-m-d H:i:s") );
                $Transacao->setDescricao("Cadastro de terreno");
                $Transacao->setItemCodigo( null );
                $Transacao->setQuantidade( 1 );
                $Transacao->setOperacao('C');
                $Transacao->setUsuarioId(  Usuario::getOnline()->getId() );
                $Transacao->setValorTotal( $valor );
                $Transacao->setValorUnidade( $valor );

                if( $Transacao->save() ){
                    if( !$Terreno->save() ){
                        $render['status'] = false;
                        $render['mensagem'] = $Terreno->daoErrorMessage;
                    }
                }else{
                    $render['mensagem'] = $Transacao->daoErrorMessage;
                }
            }else{
                if( !$Terreno->save() ){
                    $render['status'] = false;
                    $render['mensagem'] = $Terreno->daoErrorMessage;
                }
            }
            
        }
        
        $this->json($render);
    }
    
    public function excluirAction( $terrenoId = null )
    {
        $render['status'] = true;
        
        $Terreno = new Terreno($terrenoId);
        if( $terrenoId && $terrenoId !== "novo" && $Terreno->getUsuarioId() !== Usuario::getOnline()->getId() ){
            $render['status'] = false;
            $render['mensagem'] = "Operação não permitida para este usuário";
        }
        
        if( $render['status'] ){
            if( !$Terreno->delete() ){
                $render['status'] = false;
                $render['mensagem'] = $Terreno->daoErrorMessage;
            }
        }
        
        $this->json($render);
    }
    
    public function sairDeCasaAction( $terrenoId = null )
    {
        $Terreno = new Terreno($terrenoId);
        if( !$Terreno->permiteSair(Usuario::getOnline()) ){
            exit;
        }
        
        $usuario_login = Usuario::getOnline()->getLogin();
        $coordenada = $Terreno->getCoordenadaSaida();
        
        $cmd = "tp {$usuario_login} {$coordenada}";
        Rcon::send($cmd);
    }
    
    public function entrarEmCasaAction( $terrenoId = null )
    {
        $Terreno = new Terreno($terrenoId);
        if( !$Terreno->permiteEntrar(Usuario::getOnline()) ){
            die("sem permissão");
        }
        
        $usuario_login = Usuario::getOnline()->getLogin();
        $coordenada = $Terreno->getCoordenadaEntrada();
        
        $cmd = "tp {$usuario_login} {$coordenada}";
        debug($cmd, 1);
        debug(Rcon::send($cmd), 1);
    }
    
}
