<?php

use Model\LojaAdminModel as LojaAdmin;
use Model\TransacaoModel as Transacao;
use Model\UsuarioModel as UsuarioModel;

class TransacaoController extends System\MyController
{   
    public function adminAction()
    {
        $LojaAdmin = new LojaAdmin( $this->post("loja_id") );
        
        $quantidade = (Integer) $this->post("quantidade") * (Integer) $LojaAdmin->getQuantidadeMinimaVenda();
        $novaQuantidadeDisponivel = $LojaAdmin->getQuantidadeDisponivel() - $quantidade;
        
        if( $LojaAdmin->getQuantidadeDisponivel() > 0 && $novaQuantidadeDisponivel < 0 ){
            $render['status'] = false;
            $render['mensagem'] = "Unidades insuficientes na loja";
        }else{
            $LojaAdmin->setQuantidadeDisponivel( $novaQuantidadeDisponivel );

            if( $LojaAdmin->save() ){

                $Transacao = new Transacao();

                if( $LojaAdmin->getOperacao() === "C" ){
                    $Transacao->setDescricao("Venda de ".$LojaAdmin->getItemCodigo()." em loja do Server");
                    $Transacao->setOperacao( "V" );
                }else{
                    $Transacao->setDescricao("Compra de ".$LojaAdmin->getItemCodigo()." em loja do Server");
                    $Transacao->setOperacao( "C" );
                }

                $Transacao->setItemCodigo( $LojaAdmin->getItemCodigo() );
                $Transacao->setQuantidade( $quantidade );
                $Transacao->setValorUnidade( $LojaAdmin->getValorUnidade() );
                $Transacao->setUsuarioId( UsuarioModel::getOnline()->getId() );

                if( $Transacao->save() ){
                    $render['status'] = true;
                    $render['mensagem'] = "Transação efetuada com sucesso";
                }else{
                    $render['status'] = false;
                    $render['mensagem'] = $Transacao->daoErrorMessage;
                }
            }else{
                $render['status'] = false;
                $render['mensagem'] = $LojaAdmin->daoErrorMessage;
            }
            
        }
        
        $this->json($render);
    }
    
    public function getSaldoAction()
    {
        $render['saldo'] = Transacao::getSaldoUsuario(UsuarioModel::getOnline());
        $this->json($render);
    }
}
