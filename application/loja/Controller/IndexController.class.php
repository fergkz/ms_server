<?php

use Model\LojaAdminModel as LojaAdminModel;

class IndexController extends System\MyController
{   
    public function indexAction()
    {
        $render['lojas_compra'] = LojaAdminModel::getList(array(
            "dao.operacao = 'C'",
            "dao.quantidade_disponivel <> 0",
            "dao.quantidade_disponivel >= dao.quantidade_minima_venda"
        ));
        
        $render['lojas_venda'] = LojaAdminModel::getList(array(
            "dao.operacao = 'V'",
            "dao.quantidade_disponivel <> 0",
            "dao.quantidade_disponivel >= dao.quantidade_minima_venda"
        ));
        
        $this->view("index/index.twig", $render);
    }
}
