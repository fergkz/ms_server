<?php

namespace System;

use MVC\Session as Session;

class MyController extends \MVC\Controller
{
    private $forceValidate = false;
    private $getFlashData = null;
    private $flashData = null;
    
    public $ajax = false;
    public $config;
    
    public function __construct()
    {
        $this->config = new \stdClass();
        $this->config->url_call   = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
        
        $this->config->define = new \stdClass();
        $this->config->define->url_mode   = defined('url_mode') ? url_mode : null;
        $this->config->define->url_module = defined('url_module') ? url_module : null;
        
        if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
            $this->ajax = true;
        }
        $GLOBALS['page']['is_ajax'] = $this->ajax;
        
        $this->getFlashData = Session::get("flash_data");
        Session::del("flash_data");
    }
    
    public function __destruct()
    {
        Session::set("flash_data", $this->flashData);
        parent::__destruct();
    }
    
    public static function getInstance()
    {
        return new MyController();
    }

    public function flash()
    {
        $args = func_get_args();
        
        $args[0] = "flash_data_".$args[0];
        
        if( !array_key_exists(1, $args) ){
            return $this->getFlashData[$args[0]];
        }else{
            return $this->flashData[$args[0]] = $args[1];
        }
    }

    public function json( $render = array(), $validate = false )
    {
        header('Content-Type: text/html; charset=UTF-8');

        #Tratamento
        if( $validate || $this->forceValidate ){
            $errors = $this->error();
            if( $errors ){
                $render['status'] = false;
                $render['msg'] = (implode('<br/>', $errors));
                $this->error(null);
            }
        }
        
        die(json_encode($render));
    }
    
    public function view($template = "", $render = array(), $options = array())
    {
        if( !empty($this->breadcrumb) ){
            $GLOBALS['view']['breadcrumb'] = $this->breadcrumb ;
        }
        
        $View = parent::view($options);
        
        if( $template ){
            return $tpl = $View->setTemplate($template)->display($render);
        }
    
        return $View;
    }
    
    public function session()
    {
        $args = func_get_args();
        
        if( !array_key_exists(0, $args) ){
            return Session::get();
        }elseif( !array_key_exists(1, $args) ){
            return Session::get($args[0]);
        }else{
            return Session::set($args[0], $args[1]);
        }
    }
    
    public function post( $name = null )
    {
        $args = func_get_args();
        $name = $args[0];
        
        if( count($args) > 1 ){
            $res = null;
            $res = $_POST;
            foreach( $args as $arg ){
                if( empty($res[$arg]) ){
                    return null;
                }
                $res = $res[$arg];
            }
            return $res;
        }elseif( $name ){
            return !empty($_POST[$name]) ? $_POST[$name] : null;
        }else{
            return $_GET;
        }
    }
    
    public function get( $name = null )
    {
        if( $name ){
            return !empty($_GET[$name]) ? $_GET[$name] : null;
        }else{
            return $_GET;
        }
    }
    
    public function files( $name = null )
    {
        if( $name ){
            return !empty($_FILES[$name]) ? $_FILES[$name] : null;
        }else{
            return $_FILES;
        }
    }
    
    public function redirect( $url )
    {
        if( !stristr($url, url) ){
            $url = url."/".trim( ltrim($url, "\\..\/") );
        }
        header("Location: {$url}");
        exit;
    }
}