<?php

namespace System;

class MySession
{
    public static function status()
    {
        if( !session_status() ){
            session_start();
        }
    }
    
    public static function get( $key )
    {
        self::status();
        return !empty($_SESSION[$key]) ? $_SESSION[$key] : null;
    }
    
    public static function set( $key, $value )
    {
        self::status();
        $_SESSION[$key] = $value;
    }
}
