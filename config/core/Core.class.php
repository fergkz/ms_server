<?php

use System\MySession as Session;

class Core
{   
    public static function session()
    {
        $args = func_get_args();
        
        if( !array_key_exists(0, $args) ){
            return Session::get();
        }elseif( !array_key_exists(1, $args) ){
            return Session::get($args[0]);
        }else{
            return Session::set($args[0], $args[1]);
        }
    }
    
    public static function redirect( $url )
    {
        if( !stristr($url, url) ){
            $url = url."/".trim( ltrim($url, "\\..\/") );
        }
        header("Location: {$url}");
        exit;
    }
}
